﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Armaz
{
    public enum Type
    {
        FileWithName = 4,
        FileOnly = 3,
        ConnectedId = 2,
        EndOfFiles = 9999,
        KeepAlive = 5
    }
}
