﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Armaz
{
    public static class ArmazServer
    {
        public static Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        public const int BUFFER_SIZE = 1024;
        public static byte[] buffer = new byte[BUFFER_SIZE];
        public static StringBuilder sb = new StringBuilder();
        public static State state = State.ReadingType;
        public static int AmountReadInState = 0;
        public static int BitsOfType = 48;
        public static int AmountPayload = 0;
        public static Type Type;
        public static Parameters parameters = new Parameters();
        public static bool Authenticated;
        public static bool LoginFailed;
        public const string Login = "0000000100000000";


        public static Parameters ProcessTcpIp(Parameters pa)
        {
            parameters = pa;
            parameters.SentFiles = new List<FileItem>();
            socket.Connect(parameters.ArmazAdress, parameters.ArmazPort);
            SendId();
            BeginReceive();
            while (!Authenticated)
            {
                Thread.Sleep(2000);
            }
            if (LoginFailed)
            {
                socket.Close();
                return parameters;
            }
            Authenticated = false;
            BeginReceive();
            SendAllFiles(parameters);
            SendEndOfFiles();
            while (true)
            {
                if (!IsConnected(socket))
                {
                    //UnzipAllFiles();
                    return parameters;
                }
                Thread.Sleep(2000);
            }
        }

        public static bool IsConnected(Socket socket)
        {
            return socket.Connected;
        }

        private static void BeginReceive()
        {
            sb.Clear();
            buffer = new byte[BitsOfType];
            AmountReadInState = 0;
            state = State.ReadingType;
            socket.BeginReceive(
                    buffer, 0,
                    BitsOfType,
                    SocketFlags.None,
                    new AsyncCallback(OnBytesReceived), null);
        }

        public static void OnBytesReceived(IAsyncResult ar)
        {
            int read = socket.EndReceive(ar);
            switch (state)
            {
                case State.ReadingType:
                    AmountReadInState += read;
                    sb.Append(Encoding.ASCII.GetString(buffer, 0, read));
                    if (AmountReadInState != BitsOfType)
                    {
                        socket.BeginReceive(buffer, AmountReadInState, BitsOfType - AmountReadInState, SocketFlags.None,
                                         new AsyncCallback(OnBytesReceived), ar.AsyncState);
                    }
                    else
                    {
                        AmountReadInState = 0;
                        state = State.ReadingFile;
                        int count = 0;
                        if (sb.ToString().Substring(0, 2) == "\r\n")
                        {
                            Type = (Type)ConvertBin(sb.ToString().Substring(2, 16));
                            count = 18;
                        }
                        else
                        {
                            Type = (Type)ConvertBin(sb.ToString().Substring(0, 16));
                            count = 16;
                        }
                        int tail = 2;
                        if (Type == Type.FileWithName)
                        {
                            tail = 0;
                        }
                        AmountPayload = ConvertBin(sb.ToString().Substring(count, 32)) + tail;
                        buffer = new byte[AmountPayload];
                        sb.Clear();
                        socket.BeginReceive(buffer, 0, AmountPayload, SocketFlags.None,
                                         new AsyncCallback(OnBytesReceived), ar.AsyncState);
                    }
                    break;
                case State.ReadingFile:
                    AmountReadInState += read;
                    sb.Append(Encoding.ASCII.GetString(buffer, 0, read));

                    if (AmountReadInState != AmountPayload)
                    {
                        socket.BeginReceive(buffer, AmountReadInState, AmountPayload - AmountReadInState, SocketFlags.None,
                                         new AsyncCallback(OnBytesReceived), ar.AsyncState);
                    }
                    else
                    {
                        switch (Type)
                        {
                            case Type.FileOnly:
                                int fileNameSizeOnly = ConvertBin(sb.ToString().Substring(0, 32));
                                string fileNameOnly = sb.ToString().Substring(32, fileNameSizeOnly);
                                parameters.Error = Error.AZ_CONNECT;
                                parameters.ErrorDescription = fileNameOnly;
                                Authenticated = true;
                                LoginFailed = true;
                                break;
                            case Type.ConnectedId:
                                Authenticated = true;
                                break;
                            case Type.KeepAlive:
                                BeginReceive();
                                break;
                            case Type.FileWithName:
                                int fileNameSize = ConvertBin(sb.ToString().Substring(0, 32));
                                string fileName = sb.ToString().Substring(32, fileNameSize);
                                int fileSize = ConvertBin(sb.ToString().Substring(32 + fileNameSize, 32));
                                byte[] file = Encoding.ASCII.GetBytes(sb.ToString().Substring(64 + fileNameSize, fileSize));
                                var FileToAdd = new FileItem(fileName, file);
                                (parameters.SentFiles as List<FileItem>).Add(FileToAdd);
                                parameters.Error = Error.AZ_SUCESSO;
                                parameters.ErrorDescription = "Sucesso";
                                BeginReceive();
                                break;
                            case Type.EndOfFiles:
                                parameters.Error = Error.AZ_SUCESSO;
                                parameters.ErrorDescription = "Sucesso";
                                socket.Close();
                                break;
                            default:
                                parameters.Error = Error.AZ_CONNECT;
                                parameters.ErrorDescription = "Resposta de identificação desconhecida";
                                Authenticated = true;
                                LoginFailed = true;
                                socket.Close();
                                break;
                        }
                    }
                    break;
            }
        }

        private static void SendId()
        {
            if (!SendMessage(ConvertLen(1, 2), GetConnectId(), true))
            {
                parameters.Error = Error.AZ_CONNECT;
                parameters.ErrorDescription = "Falhou o envio do pacote id 0x0001";
            }
        }
        public static int ConvertBin(string s)
        {
            int res = 0;
            for (int k = s.Length - 8; k >= 0; k -= 8)
            {
                foreach (char c in s.Substring(k, 8))
                {
                    res <<= 1;
                    if (c != '0')
                        res |= 1;
                }
            }
            return res;
        }
        public static string ConvertLen(int n, int size)
        {
            string ret = "";
            for (int k = 0; k < size; ++k)
            {
                int b = n & 0xff;
                n >>= 8;
                ret += Convert.ToString(b, 2).PadLeft(8, '0');
            }
            return ret;
        }

        //Returns a bool wheter sent the entire message or not
        public static bool SendMessage(string type, string message, bool NeedsCB)
        {
            string entireMessage = "";
            if (type == Login)
            {
                entireMessage = type + ConvertLen(message.Length + 32, 4) + ConvertLen(message.Length, 4) + message;
            }
            else
            {
                entireMessage = type + ConvertLen(message.Length, 4) + message;
            }
            if (NeedsCB)
            {
                entireMessage += "\r\n";
            }
            var buffer = Encoding.ASCII.GetBytes(entireMessage);
            if (socket.Send(buffer, buffer.Length, SocketFlags.None) != buffer.Length)
            {
                return false;
            }
            return true;
        }

        public static void SendAllFiles(Parameters pa)
        {
            for (int i = 0; i < pa.ReceivedFiles.Count; i++)
            {
                string payload = ConvertLen(pa.ReceivedFiles.ElementAt(i).FileName.Length, 4) + pa.ReceivedFiles.ElementAt(i).FileName
                    + ConvertLen(pa.ReceivedFiles.ElementAt(i).FileContent.Length, 4) + Encoding.ASCII.GetString(pa.ReceivedFiles.ElementAt(i).FileContent);
                if (!SendMessage(ConvertLen(4, 2), payload, false))
                {
                    parameters.Error = Error.AZ_CONNECT;
                    parameters.ErrorDescription = "Falhou o envio do pacote id 0x0004";
                }
            }
        }

        public static void SendEndOfFiles()
        {
            if (parameters.Error == Error.AZ_SUCESSO)
            {
                if (!SendMessage(ConvertLen(9999, 2), "", true))
                {
                    parameters.Error = Error.AZ_CONNECT;
                    parameters.ErrorDescription = "Falhou o envio do pacote id 9999";
                }
            }
            else
            {
                socket.Close();
            }
        }

        public static string GetConnectId()
        {
            string s = parameters.ClientNumber;
            if (parameters.ClientPassword != null)
            {
                s += "," + parameters.ClientPassword;
            }
            return s;
        }

        //UnzipFiles?
        //private void UnzipAllFiles()
        //{
        //    for (int i = 0; i < parameters.SentFiles.Count; i++)
        //    {
        //        if (IsZip((parameters.SentFiles as List<FileItem>)[i].FileName))
        //        {
        //            UncompressPayload((parameters.SentFiles as List<FileItem>)[i].FileContent);
        //            (parameters.SentFiles as List<FileItem>).RemoveAt(i);
        //        }
        //    }
        //}
        //private void UncompressPayload(byte[] data)
        //{
        //    Stream stream = new MemoryStream(data);
        //    using (ZipFile z = ZipFile.Read(stream))
        //    {
        //        foreach (ZipEntry zEntry in z)
        //        {
        //            MemoryStream tempS = new MemoryStream();
        //            zEntry.Extract(tempS);
        //            (parameters.SentFiles as List<FileItem>).Add(new FileItem(zEntry.FileName, tempS.ToArray()));
        //        }
        //    }
        //}

        //private bool IsZip(string fileName)
        //{
        //    if (fileName.Substring(fileName.Length - 4).ToLowerInvariant() == ".zip")
        //    {
        //        return true;
        //    }
        //    return false;

        //}
    }
}
