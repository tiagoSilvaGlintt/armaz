﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Armaz
{
    public class FileItem
    {
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }

        public FileItem(string fileName, byte[] fileContent)
        {
            FileName = fileName;
            FileContent = fileContent;
        }
    }
}
