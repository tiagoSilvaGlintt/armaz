﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Armaz
{
    public enum Error{
        AZ_SUCESSO,     //Sucesso
        AZ_PARERR,      //Parâmetros incorrectos
        AZ_SERVER,      //Connect com servidor de comunicações falhou
        AZ_PROFILE,     //Profile de dial-up não existe
        AZ_DIAL,        //Dial Falhou
        AZ_CONNECT,     //Connect com armazenista falhou
        AZ_TIMEOUT,     //Timeout em comunicação
        AZ_FILEREAD,    //Erro na leitura de file do disco
        AZ_FILEWRITE,   //Erro na escrita de file para o disco
        AZ_SPACE,       //Espaço insuficiente no buffer de retorno dos nomes
        AZ_BREAK        //Operação interrompida pelo utilizador
    };
}
