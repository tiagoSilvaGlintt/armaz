﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Armaz
{
    public class Parameters
    {
        public string ArmazAdress { get; set; }
        public int ArmazPort { get; set; }
        public string ClientNumber { get; set; }
        public string ClientPassword { get; set; }
        public Error Error { get; set; }
        public string ErrorDescription { get; set; }
        public ICollection<FileItem> ReceivedFiles { get; set; }
        public ICollection<FileItem> SentFiles { get; set; }
    }
}
