﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Armaz
{

    public class ArmazWrapper
    {
        public string ServerTcpAdress { get; set; }
        public int ServerTcpPort { get; set; }
        public string ArmaTcpAdress { get; set; }
        public int ArmaTcpPort { get; set; }
        public string SentFile { get; set; }
        public StringBuilder ReceivedFile = new StringBuilder(512);
        public int ReceivedFileSize { get; set; }
        public StringBuilder ErrorText = new StringBuilder(512);
        public int ErrorSize { get; set; }


        [DllImport("armazcli.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern int armazcli(string serv_tcp_addr, int serv_tcp_port, string arma_tcp_addr,
                                            int arma_tcp_port, int tipo_ligacao, int timeout, string profile, string n_telefone, string username, string password,
                                            string n_cliente, string passwd_cliente, string fich_env, StringBuilder fich_rec, int recebido_size, StringBuilder erro_texto, int erro_size);


        public int ArmazDllMethod(string ServerTcpAdress, int ServerTcpPort, string ArmaTcpAdress, int ArmaTcpPort, string SentFile, StringBuilder ReceivedFile, int ReceivedFileSize, StringBuilder ErrorText, int ErrorSize)
        {
            return armazcli(ServerTcpAdress, ServerTcpPort, ArmaTcpAdress,
                                            ArmaTcpPort, 0, 0, "", "", "", "",
                                            "", "", SentFile, ReceivedFile, ReceivedFileSize, ErrorText, ErrorSize);
        }

    }
}
