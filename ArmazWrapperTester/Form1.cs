﻿using Armaz;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ArmazWrapperTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //var testeObject = new ArmazWrapper
            //{
            //    ArmaTcpAdress = "127.0.0.1",
            //    ArmaTcpPort = 4444,
            //    ServerTcpAdress = "127.0.0.1",
            //    ServerTcpPort = 5555,
            //    SentFile = "Olá ",
            //    ReceivedFile = new StringBuilder(512),
            //    ErrorSize = 512,
            //    ErrorText = new StringBuilder(512),
            //    ReceivedFileSize = 512
            //};
            //var result = testeObject.ArmazDllMethod(testeObject.ServerTcpAdress, testeObject.ServerTcpPort, testeObject.ArmaTcpAdress, testeObject.ArmaTcpPort,
            //       testeObject.SentFile, testeObject.ReceivedFile, testeObject.ReceivedFileSize, testeObject.ErrorText, testeObject.ErrorSize);

            var file = new FileItem("femodem.dad", File.ReadAllBytes(@"C:\Users\tiago.silva\Downloads\distri3\fich\armaz\HIST\F000016390_2014121809242927_EO10001.DAD"));
            var file2 = new FileItem("femodem.dad", File.ReadAllBytes(@"C:\Users\tiago.silva\Downloads\distri3\fich\armaz\HIST\F000016390_20160128102122521_EO10001.DAD"));
            var file3 = new FileItem("femodem.dad", File.ReadAllBytes(@"C:\Users\tiago.silva\Downloads\distri3\fich\armaz\HIST\F000016390_2014121809242927_EO10001.DAD"));
            var file4 = new FileItem("femodem.dad", File.ReadAllBytes(@"C:\Users\tiago.silva\Downloads\distri3\fich\armaz\HIST\F000016390_2014121809242927_EO10001.DAD"));
            var parameters = new Parameters
            {
                ArmazAdress = "localhost",
                ArmazPort = 1900,
                ClientNumber = "F000000007",
                ClientPassword = "AAAAAAAAAA"
            };
            parameters.ReceivedFiles = new List<FileItem>();
            parameters.ReceivedFiles.Add(file);
            parameters.ReceivedFiles.Add(file2);
            parameters.ReceivedFiles.Add(file3);
            parameters.ReceivedFiles.Add(file4);
            ArmazServer.ProcessTcpIp(parameters);
        }
    }
}
